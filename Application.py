import sys
if sys.version_info[0] == 2:  # Just checking your Python version to import Tkinter properly.
	print('Wrong Python version, you should use Python3')
	sys.exit()

from tkinter import *
from tkinter import ttk

import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure

from JuiceTank import JuiceTank
from Pid import Pid

class Fullscreen_Window:

	def __init__(self):
		self.juiceTank = JuiceTank()
		self.pid = Pid(P=15, I=2.5, D=0,
		               maxOut = self.juiceTank.getInputWaterFlowMaximal(),
		               minOut = self.juiceTank.getInputWaterFlowMinimal())
		self.pid.setPoint(0.6)

		self.tk = Tk()
		self.tk.wm_attributes('-fullscreen','true')
		
		self.plotFrame = Frame(self.tk, width=480, height=202)
		self.topFrame = Frame(self.tk, width=480, height=70, relief='raised', borderwidth=2)
		self.switchableFrame = Frame(self.topFrame, width=410, height=66)
		self.whichFrameIsVisible = 'Control'
		
		self.controlFrame = Frame(self.switchableFrame, width=420, height=66)
		self.pidFrame = Frame(self.switchableFrame, width=420, height=66)
		
		for frame in [self.topFrame, self.plotFrame]:
			frame.pack(expand=True, fill=BOTH)
			frame.pack_propagate(0)
		
		self.isFullscreen = True
		self.tk.bind("<F11>", self.toggle_fullscreen)
		self.tk.bind("<Escape>", self.end_application)
		
		self.switchLeft = ttk.Button(self.topFrame, text='<', command=self.switchTopFrame, width=2)
		self.switchLeft.pack(side=LEFT, fill=Y)
		
		self.switchableFrame.pack_propagate(0)
		self.switchableFrame.pack(side=LEFT, fill=BOTH)
		
		self.controlFrame.pack_propagate(0)
		self.controlFrame.pack(side=LEFT, fill=BOTH)
		
		self.switchRight = ttk.Button(self.topFrame, text='>', command=self.switchTopFrame, width=2)
		self.switchRight.pack(side=RIGHT, fill=Y)
		
		self.labelTop = ttk.Label(self.controlFrame, text="Top")
		self.labelTop.pack()

		self.labelBottom = ttk.Label(self.controlFrame, text="Bottom")
		self.labelBottom.pack()
		
		# Scale
		self.labelScaleDesc = ttk.Label(self.controlFrame, text='Setpoint concentration value:')
		self.labelScaleDesc.pack(side=LEFT, padx=(5, 5), pady=(2, 2))
		
		self.scaleEntryVar = StringVar(value="{:.2f}".format(self.pid.getPoint()))
		self.scaleEntry = ttk.Entry(self.controlFrame, width=10, state="readonly", textvariable=self.scaleEntryVar)
		self.scaleEntry.pack(side=LEFT, padx=(5, 5), pady=(2, 2))
		
		self.scaleVar = DoubleVar(value=self.pid.getPoint())
		self.scale = ttk.Scale(self.controlFrame, variable=self.scaleVar, orient=HORIZONTAL, length=180, from_=0.0, to=1.0)
		self.scale.pack(side=LEFT)
		
		# PidFrame -- internal frames
		self.pFrame = Frame(self.pidFrame, width=420, height=20)
		self.iFrame = Frame(self.pidFrame, width=420, height=20)
		self.dFrame = Frame(self.pidFrame, width=420, height=20)
		for frame in [self.pFrame, self.iFrame, self.dFrame]:
			frame.pack_propagate( 0 )
			frame.pack( fill=X )
		
		# Scale - Pid :: P
		self.pScaleVar = DoubleVar(value=self.pid.Kp)
		self.pScale = ttk.Scale(self.pFrame, variable=self.pScaleVar, orient=HORIZONTAL, length=220, from_=0.0, to=100.0)
		self.pScale.pack(side=RIGHT)
		
		self.pScaleEntryVar = StringVar(value="{:.2f}".format(self.pid.Kp))
		self.pScaleEntry = ttk.Entry(self.pFrame, width=10, state="readonly", textvariable=self.pScaleEntryVar)
		self.pScaleEntry.pack(side=RIGHT, padx=(5, 5), pady=(2, 2))
		
		self.pLabel = ttk.Label( self.pFrame, text='Proportional' )
		self.pLabel.pack(side=RIGHT, padx=(5, 5), pady=(2, 2))
		
		# Scale - Pid :: I
		self.iScaleVar = DoubleVar(value=self.pid.Ki)
		self.iScale = ttk.Scale(self.iFrame, variable=self.iScaleVar, orient=HORIZONTAL, length=220, from_=0.0, to=10.0)
		self.iScale.pack(side=RIGHT)
		
		self.iScaleEntryVar = StringVar(value="{:.2f}".format(self.pid.Ki))
		self.iScaleEntry = ttk.Entry(self.iFrame, width=10, state="readonly", textvariable=self.iScaleEntryVar)
		self.iScaleEntry.pack(side=RIGHT, padx=(5, 5), pady=(2, 2))
		
		self.iLabel = ttk.Label( self.iFrame, text='Integral' )
		self.iLabel.pack(side=RIGHT, padx=(5, 5), pady=(2, 2))
		
		# Scale - Pid :: D
		self.dScaleVar = DoubleVar(value=self.pid.Kd)
		self.dScale = ttk.Scale(self.dFrame, variable=self.dScaleVar, orient=HORIZONTAL, length=220, from_=0.0, to=10.0)
		self.dScale.pack(side=RIGHT)
		
		self.dScaleEntryVar = StringVar(value="{:.2f}".format(self.pid.Kd))
		self.dScaleEntry = ttk.Entry(self.dFrame, width=10, state="readonly", textvariable=self.dScaleEntryVar)
		self.dScaleEntry.pack(side=RIGHT, padx=(5, 5), pady=(2, 2))
		
		self.dLabel = ttk.Label( self.dFrame, text='Derivative' )
		self.dLabel.pack(side=RIGHT, padx=(5, 5), pady=(2, 2))
		
		# Plot tests
		self.f = Figure(figsize=(5, 5), dpi=100)
		self.a = self.f.add_subplot(111)
		self.plot1, = self.a.plot([0],[0])
		self.plot2, = self.a.plot([0],[0])

		self.canvas = FigureCanvasTkAgg(self.f, self.plotFrame)
		self.canvas.draw()
		self.canvas.get_tk_widget().pack(fill=BOTH, expand=True)

	def switchTopFrame(self, event=None):
		if self.whichFrameIsVisible == 'Pid':
		
			self.pidFrame.pack_forget()
			self.controlFrame.pack()
		
			self.whichFrameIsVisible = 'Control'
		else:
		
			self.controlFrame.pack_forget()
			self.pidFrame.pack()
		
			self.whichFrameIsVisible = 'Pid'
		
	def toggle_fullscreen(self, event=None):
		if self.isFullscreen:
			self.tk.wm_attributes("-fullscreen", 'false')
			self.isFullscreen = False
		else:
			self.tk.wm_attributes("-fullscreen", 'true')
			self.isFullscreen = True

	def end_application(self, event=None):
		self.tk.destroy()

plot1X = []
plot1Y = []
plot2X = []
plot2Y = []
w = Fullscreen_Window()
prevSetpoint = w.pid.getPoint()
prevKP = w.pScaleVar.get()
prevKI = w.iScaleVar.get()
prevKD = w.dScaleVar.get()
def timerCallback():
	global plot1X, plot1Y, plot2X, plot2Y
	global prevSetpoint
	global prevKP, prevKI, prevKD

	# Scale widget setpoint read
	currSetpoint = w.scaleVar.get()
	if prevSetpoint != currSetpoint:
		w.pid.setPoint(currSetpoint)
		w.scaleEntryVar.set( "{:.2f}".format( currSetpoint ) )
		prevSetpoint = currSetpoint
	
	# PID tuning reaction
	currKP = w.pScaleVar.get()
	if prevKP != currKP:
		w.pid.Kp = currKP
		w.pScaleEntryVar.set( "{:.2f}".format( currKP ) )
		prevKP = currKP
		
	currKI = w.iScaleVar.get()
	if prevKI != currKI:
		w.pid.Ki = currKI
		w.iScaleEntryVar.set( "{:.2f}".format( currKI ) )
		prevKI = currKI
		
	currKD = w.dScaleVar.get()
	if prevKD != currKD:
		w.pid.Kd = currKD
		w.dScaleEntryVar.set( "{:.2f}".format( currKD ) )
		prevKD = currKD
	
	# Operations on model
	u = w.pid.update( w.juiceTank.getConcentration() )
	w.juiceTank.setInputWaterFlow( u )
	
	#for i in range(10):
	w.juiceTank.simulateOneIteration()
		
	currentTime = w.juiceTank.getCurrentTime()
	for plotX in [plot1X, plot2X]:
		plotX.append( currentTime )
		
	plot1Y.append( w.juiceTank.getConcentration() )
	plot2Y.append( w.pid.getPoint() )
	
	# Printing
	w.labelTop['text'] = str(w.pid)
	w.labelBottom['text'] = 'u: ' + "{:.2f}".format( u ) + '; qWater: ' + "{:.2f}".format( w.juiceTank.getInputWaterFlow() ) + '; qJuice: ' + "{:.2f}".format( w.juiceTank.getInputJuiceFlow() )
	
	# Plotting
	MAX_LEN = 25
	if len(plot1X) > MAX_LEN:
		plot1X = plot1X[-MAX_LEN:]
		plot1Y = plot1Y[-MAX_LEN:]
		plot2X = plot2X[-MAX_LEN:]
		plot2Y = plot2Y[-MAX_LEN:]
	
	w.a.set_xlim(min(min(plot1X), min(plot2X)) - 0.5, max(max(plot1X), max(plot2X)) + 0.5)
	w.a.set_ylim(min(min(plot1Y), min(plot2Y)) - 0.4, max(max(plot1Y), max(plot2Y)) + 0.4)
	w.plot1.set_data(plot1X, plot1Y)
	w.plot2.set_data(plot2X, plot2Y)
	w.canvas.draw()
	w.tk.after(10, timerCallback)
		
if __name__ == '__main__':
	w.tk.after(0, timerCallback)
	w.tk.mainloop()
