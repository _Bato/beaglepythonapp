import sys
import serial
import glob

class JuiceTank:
	def __init__(self):
		self.serial = None
		self.connectToBoard()
		self.reset()
		
	def simulateOneIteration(self):
		self.sendAndRecv('step')
		
	def reset(self):
		self.sendAndRecv('reset')
		
	def getConcentration(self):
		return self.readValue('gct')
		
	def getCurrentTime(self):
		return self.readValue('gtime')
		
	def getSimulationStep(self):
		return self.readValue('gstep')
		
	def getFlow(self):
		return self.readValue('gqt')
		
	def getVolume(self):
		return self.readValue('gvt')
		
	def getInputJuiceConcentration(self):
		return self.readValue('gc1')
		
	def getInputJuiceFlow(self):
		return self.readValue('gq1')
		
	def getInputWaterConcentration(self):
		return self.readValue('gc2')
		
	def getInputWaterFlow(self):
		return self.readValue('gq2')
		
	def getInputWaterFlowMaximal(self):
		return self.readValue('gq2+')
		
	def getInputWaterFlowMinimal(self):
		return self.readValue('gq2-')
	
	def setInputWaterFlow(self, value):
		self.sendAndRecv( 'sq2', value )
	
	def connectToBoard(self):
		if self.serial:
			self.serial.close()
	
		availableSerials = glob.glob('/dev/ttyACM*')
		if not availableSerials:
			print('No juice tank simulator connected')
			sys.exit()

		for serialName in availableSerials:
			try:
				self.serial = serial.Serial(serialName)
				print('Succesfully connected to:', self.serial.name)
				return
			except (OSError, serial.SerialException):
				print('Cannot connect to:', serialName)
				sys.exit()

	def sendAndRecv(self, message, value=None):
		message += '!'
		
		if value != None:
			message += "{:.4f}".format(value) + '!'
		
		while True:
			try:
				self.serial.write(message.encode())
				break
			except serial.serialutil.SerialException:
				self.connectToBoard()

		#print('Read: ', sep='', end='')
		output = ""
		while True:
			try:
				c = self.serial.read(1).decode('utf-8')
			except serial.serialutil.SerialException:
				self.connectToBoard()
			#print(c, sep='', end='')
			if c == '#':
				break
			output += c

		return output
	
	def readValue(self, message):
		while True:
			answer = self.sendAndRecv(message)
			
			try:
				answer = float(answer)
				return answer
			except ValueError:
				pass
	